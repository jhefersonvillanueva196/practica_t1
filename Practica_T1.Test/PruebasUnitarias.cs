﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Practica_T1.Test
{
    class PruebasUnitarias
    {
        [Test]
        public void Caso1()
        {
            var utils = new Funciones();
            int numero = 1;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("I", resultado);
        }

        [Test]
        public void Caso2()
        {
            var utils = new Funciones();
            int numero = 20;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("XX", resultado);
        }
        [Test]
        public void Caso3()
        {
            var utils = new Funciones();
            int numero = 35;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("XXXV", resultado);
        }
        [Test]
        public void Caso4()
        {
            var utils = new Funciones();
            int numero = 80;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("LXXX", resultado);
        }
        [Test]
        public void Caso5()
        {
            var utils = new Funciones();
            int numero = 350;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("CCCL", resultado);
        }
        [Test]
        public void Caso6()
        {
            var utils = new Funciones();
            int numero = 180;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("CLXXX", resultado);
        }
        [Test]
        public void Caso7()
        {
            var utils = new Funciones();
            int numero = 135;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("CXXXV", resultado);
        }
        [Test]
        public void Caso8()
        {
            var utils = new Funciones();
            int numero = 59;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("LIX", resultado);
        }
        [Test]
        public void Caso9()
        {
            var utils = new Funciones();
            int numero = 552;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("DLII", resultado);
        }
        [Test]
        public void Caso10()
        {
            var utils = new Funciones();
            int numero = 801;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("DCCCI", resultado);
        }
        [Test]
        public void Caso11()
        {
            var utils = new Funciones();
            int numero = 301;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("CCCI", resultado);
        }
        [Test]
        public void Caso12()
        {
            var utils = new Funciones();
            int numero = 520;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("DXX", resultado);
        }
        [Test]
        public void Caso13()
        {
            var utils = new Funciones();
            int numero = 795;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("DCCXCV", resultado);
        }
        [Test]
        public void Caso14()
        {
            var utils = new Funciones();
            int numero = 1000;
            var resultado = utils.Representacion(numero);
            Assert.AreEqual("M", resultado);
        }
    }
}
