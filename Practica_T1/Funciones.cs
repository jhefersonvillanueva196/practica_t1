﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica_T1
{
    public class Funciones
    {
        public string Representacion(int numero)
        {
            int Miles = 0, Resto = 0, Cen = 0, Dec = 0, Uni = 0, N = 0;

            Miles = numero / 1000;
            Resto = numero % 1000;
            Cen = Resto / 100;
            Resto = Resto % 100;
            Dec = Resto / 10;
            Resto = Resto % 10;
            Uni = Resto;
            String romano = "";

            switch (Miles)
            {
                case 1:
                    romano = romano + ("M");
                    break;
                case 2:
                    romano = romano + ("MM");
                    break;
                case 3:
                    romano = romano + ("MMM");
                    break;
            }
            switch (Cen)
            {
                case 1:
                    romano = romano + ("C");
                    break;
                case 2:
                    romano = romano + ("CC");
                    break;
                case 3:
                    romano = romano + ("CCC");
                    break;
                case 4:
                    romano = romano + ("CD");
                    break;
                case 5:
                    romano = romano + ("D");
                    break;
                case 6:
                    romano = romano + ("DC");
                    break;
                case 7:
                    romano = romano + ("DCC");
                    break;
                case 8:
                    romano = romano + ("DCCC");
                    break;
                case 9:
                    romano = romano + ("CM");
                    break;
            }
            switch (Dec)
            {
                case 1:
                    romano = romano + ("X");
                    break;
                case 2:
                    romano = romano + ("XX");
                    break;
                case 3:
                    romano = romano + ("XXX");
                    break;
                case 4:
                    romano = romano + ("XL");
                    break;
                case 5:
                    romano = romano + ("L");
                    break;
                case 6:
                    romano = romano + ("LX");
                    break;
                case 7:
                    romano = romano + ("LXX");
                    break;
                case 8:
                    romano = romano + ("LXXX");
                    break;
                case 9:
                    romano = romano + ("XC");
                    break;
            }
            switch (Uni)
            {
                case 1:
                    romano = romano + ("I");
                    break;
                case 2:
                    romano = romano + ("II");
                    break;
                case 3:
                    romano = romano + ("III");
                    break;
                case 4:
                    romano = romano + ("IV");
                    break;
                case 5:
                    romano = romano + ("V");
                    break;
                case 6:
                    romano = romano + ("VI");
                    break;
                case 7:
                    romano = romano + ("VII");
                    break;
                case 8:
                    romano = romano + ("VIII");
                    break;
                case 9:
                    romano = romano + ("IX");
                    break;
            }

            return romano;
        }
    }
}
