﻿using System;

namespace Practica_T1
{
    class Program
    {
        static void Main(string[] args)
        {
            var utils = new Funciones();
            Console.WriteLine("Ingresa un numero: ");
            int num = int.Parse(Console.ReadLine());
            string romanos = utils.Representacion(num);
            Console.WriteLine(romanos);
        }
    }
}
